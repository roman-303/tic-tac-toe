import React from 'react';
import ReactDOM from 'react-dom';
import Board from './Components/Board'
import Game from './Components/Game'
import Square from './Components/Square'
import './index.css';

// only for development purposes!!
if (module.hot) {
  module.hot.accept()
}
// end

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
